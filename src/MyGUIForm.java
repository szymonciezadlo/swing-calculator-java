import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;
import java.awt.Toolkit;

public class MyGUIForm extends JFrame implements ActionListener {
    private JPanel mainPanel = new JPanel();
    private JPanel rightPanel = new JPanel();
    private JTextArea textArea = new JTextArea("TextArea");
    private JTextField textField = new JTextField("TextField");

    private JList list = new JList();
    private JButton evalButton = new JButton("Evaluate");

    private JLabel label = new JLabel("Label");

    MyGUIForm() {
        setTitle("Kalkulator");
        add(mainPanel);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();
        int xPos = (dim.width / 2 - (getWidth() / 2));
        int yPos = (dim.height / 2 - (getHeight() / 2));
        setLocation(xPos, yPos);
        evalButton.setBounds(700, 300, 50, 50);
        mainPanel.add(evalButton);
        evalButton.setToolTipText("It's a button");
        //mainPanel.add(menu);
        mainPanel.add(label);
        mainPanel.add(textArea);
        mainPanel.add(textField);
        evalButton.addActionListener(this);

        //add(rightPanel);
        mainPanel.add(list);
        setSize(800, 400);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == evalButton) {
            textArea.append("Button Clicked");

        }
    }
}