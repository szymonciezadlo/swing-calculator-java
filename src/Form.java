import org.mariuszgromada.math.mxparser.Expression;


import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;

public class Form extends JFrame{
    private JPanel rootPanel;
    private JTextArea result;
    private JList list1;
    private JButton button1;
    private JTextField enterText;
    private Functions last=new Functions("Last Result","");

    public Form(){
        add(rootPanel);
        this.setSize(1000, 500);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        enterText.requestFocus();

        DefaultListModel<Functions> listModel = new DefaultListModel<>();
        listModel.addElement(new Functions("sinus","sin()"));
        listModel.addElement(new Functions("cosinus","cos()"));
        listModel.addElement(new Functions("Log o podstawie 10","log10"));
        listModel.addElement(new Functions("Arcsinus","asin()"));
        listModel.addElement(new Functions("Legendre's constant","[B'L]"));
        listModel.addElement(new Functions("e","e"));
        listModel.addElement(new Functions("Omega constant","[Om]"));
        listModel.addElement(new Functions("modulo","#"));
        listModel.addElement(new Functions("silnia","!"));
        listModel.addElement(new Functions("potęga","^"));
        listModel.addElement(last);
        list1.setModel(listModel);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                enter();
            }
        });

        enterText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_ENTER)enter();
                if(e.getKeyCode()==KeyEvent.VK_UP)enterText.setText(last.getCode());
            }
        });
        list1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount()==2){
                    enterText.setText(enterText.getText()+listModel.elementAt(list1.getAnchorSelectionIndex()).getCode());
                    if(enterText.getText().endsWith(")"))enterText.setCaretPosition(enterText.getCaretPosition()-1);
                    enterText.requestFocus();
                }

            }
        });

        //JMenu

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Options");
        JMenuItem resetItem = new JMenuItem("Reset");


        resetItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                result.setText(null);
                last.setCode("");
            }
        });

        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        menu.add(resetItem);
        menu.add(exitItem);
        menuBar.add(menu);
        this.setJMenuBar(menuBar);


    }
    void enter(){
        Expression expression=new Expression(enterText.getText());
        if(expression.checkSyntax()) {
            result.append("\n" + expression.getExpressionString() + '=' + Double.toString(expression.calculate()));
            last.setCode("" + expression.calculate());

        }
        else {
            JOptionPane.showMessageDialog(null, expression.getErrorMessage(), "syntax error", JOptionPane.ERROR_MESSAGE);
            last.setCode("");
        }
        enterText.setText("");
        enterText.requestFocus();
    }

}



