public class Functions {
    private String name;
    private String code;

    Functions(String name, String code) {
        this.name = name;
        this.code = code;
    }

    String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return name;
    }
}
